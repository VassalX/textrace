# Text Race
Game in which players can connect to a session and type text. The player who types the text first is the winner. Each player can see their progress and progress of other players in real time.

![Screenshot](./public/screenshot.png)

## Start server
```npm start```
## Connect to server
```localhost:3030```