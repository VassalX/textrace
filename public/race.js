window.onload = () => {
    const jwt = localStorage.getItem('jwt');
    let inGame = false;
    let prevT = '';
    let curT = '';
    let nextT = '';
    let letNum = 0;

    if (!jwt) {
        location.replace('/')
    } else {
        console.log("You're okay");
        let socket = io.connect('http://localhost:3030');

        socket.on('connect', function() {
            socket
                .emit('authenticate', { token: jwt }) //send the jwt
                .on('authenticated', function() {
                    window.onkeypress = function(e) {
                        const key = String.fromCharCode(e.keyCode || e.charCode);
                        if (key === curT) {
                            socket.emit('key', key);
                            prevT = prevT + curT;
                            curT = nextT[0];
                            nextT = nextT.substr(1);
                            enterText(prevT, curT, nextT);
                        }
                    }
                    socket.on('time', setWaitRaceTime)
                        .on('disconnect', disconnectPlayer)
                        .on('start', startGame)
                        .on('finish', finishGame)
                        .on('keypress', addKeyPress)
                        .on('comment', commentShow)
                })
                .on('unauthorized', function(msg) {
                    console.log("unauthorized: " + JSON.stringify(msg.data));
                    throw new Error(msg.data.type);
                })
        });
    }

    function commentShow(comment) {
        document.querySelector('.comment-text').innerHTML = comment;
    }

    function addKeyPress(id) {
        console.log(id);
        let progress = document.getElementById(id).querySelector('progress');
        let val = parseInt(progress.getAttribute('value')) + 1;
        progress.setAttribute('value', val);
    }

    function setWaitRaceTime({ state, timeRace, timeWait }) {
        if (state === 'wait') {
            document.getElementById('time-race-wrap').style.display = 'none';
            document.getElementById('time-wait').innerText = timeWait + " sec";
        }
        if (state === 'race') {
            document.getElementById('time-race-wrap').style.display = 'block';
            document.getElementById('time-wait').innerText = (timeWait + timeRace) + " sec";
            document.getElementById('time-race').innerText = timeRace + " sec";

        }
    }

    function disconnectPlayer({ id }) {
        document.getElementById(id).classList.add('disconnect');
    }

    function startGame({ players, text }) {
        inGame = true;
        letNum = 0;
        leaders = document.getElementById("leaders");
        leaders.innerHTML = '';
        players.forEach(player => {
            let leadEl = document.createElement('div');
            leadEl.className = 'leader';
            leadEl.innerHTML = `
                <span class='name'>${player.name}</span>
                <progress class='progress' max=${text.length} value="0"></progress>`
            leadEl.setAttribute('id', player.id);
            leaders.appendChild(leadEl);
        });
        leaders.style.display = 'block';
        document.querySelector(".race-text").style.display = "block";
        prevT = '';
        curT = text[0];
        nextT = text.substr(1);
        enterText("", text[0], text.substr(1));
    }

    function enterText(prev, cur, next) {
        document.querySelector(".race-text").innerHTML = `<span class="prev">${prev}</span>
        <span class="current">${(cur || '')}</span>
        <span class="next">${next}</span>`
    }

    function finishGame() {
        inGame = false;
        document.querySelector(".race-text").style.display = "none";
    }
}