window.onload = () => {
    const loginBtn = document.getElementById("loginBtn");
    const loginField = document.getElementById("login");
    const passwordField = document.getElementById("password");

    loginBtn.addEventListener('click', ev => {
        fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: loginField.value,
                password: passwordField.value
            })
        }).then(res => {
            res.json().then(body => {
                console.log(body);
                if (body.auth) {
                    localStorage.setItem('jwt', body.token);
                    location.replace('/race');
                } else {
                    console.log('auth failed');
                }
            })
        }).catch(err => {
            console.log('request went wrong');
        })
    })
}