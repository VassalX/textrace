const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const socketioJwt = require('socketio-jwt');
const users = require('./data/users.json');
const texts = require('./data/texts.json');
require('./passport.config.js');

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

server.listen(3030);

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.get('/race', function(req, res) {
    res.sendFile(path.join(__dirname, 'public/race.html'));
});

app.post('/login', function(req, res) {
    const userFromReq = req.body;
    const userInBD = users.find(user => user.login === userFromReq.login);
    if (userInBD && userInBD.password === userFromReq.password) {
        const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
        res.status(200).json({ auth: true, token });
    } else {
        res.status(401).json({ auth: false });
    }
})

const timeWait = 5; // time in sec
let curTimeWait = timeWait;
const timeRace = 35;
let curTimeRace = timeRace;
const states = {
    stop: 'stop',
    wait: 'wait',
    race: 'race'
}
let state = states.stop;
let timerId;
let commentTimerId;
let players = [];
let curText;
let commented30Keys = false;

io.sockets
    .on('connection', socketioJwt.authorize({
        secret: 'someSecret',
        timeout: 15000 // 15 seconds to send the authentication message
    })).on('authenticated', function(socket) {
        socket.join('wait');
        joinWaitingRoom();
        players.push({
            id: socket.id,
            name: socket.decoded_token.login,
            keys: 0
        });
        socket.on('disconnect', () => {
                const discPlayer = players.find(player => player.id === socket.id);
                players = players.filter(player => player.id !== socket.id);
                io.to('race').emit('disconnect', discPlayer);
                let clients = Object.keys(io.sockets.sockets);
                if (clients.length < 2) {
                    clearInterval(timerId);
                    state = states.stop;
                    curTimeWait = timeWait;
                    curTimeRace = timeRace;
                }
            })
            .on('key', key => {
                io.to('race').emit('keypress', socket.id);
                let index = players.findIndex(player => player.id === socket.id);
                players[index].keys++;
                if (players[index].keys === curText.length) {
                    commentFinishPlayer(players[index]);
                }
                if (players[index].keys === (curText.length - 30) && !commented30Keys) {
                    comment30Keys();
                }
            });
    });

function joinWaitingRoom() {
    let clients = Object.keys(io.sockets.sockets);

    if (clients.length >= 2) {
        if (state === states.stop) {
            state = states.wait;
            timerId = setInterval(waitNextRace, 1000);
        }
    } else {
        clearInterval(timerId);
        state = states.stop;
        curTimeWait = timeWait;
        curTimeRace = timeRace;
    }
}

function waitNextRace() {
    io.emit('time', {
        state: state,
        timeRace: curTimeRace,
        timeWait: curTimeWait
    });
    if (state === states.race) {
        curTimeRace--;
        if (curTimeRace <= 0) {
            curTimeRace = timeRace;
            state = states.wait;
            finishRace();
        }
    } else if (state === states.wait) {
        curTimeWait--;
        if (curTimeWait <= 0) {
            curTimeWait = timeWait;
            state = states.race;
            let sockets = io.sockets.sockets;
            for (let sid in sockets) {
                let socket = sockets[sid];
                socket.leave('wait');
                socket.join('race');
            }
            startRace();
        }
    }
}

function enableComments() {
    commented30Keys = false;
    let comment = "Вітаю всіх з початком перегонів!\n Оголошую список учасників:";
    for (let i = 0; i < players.length - 1; i++) {
        comment += ` ${players[i].name},`;
    }
    comment += ` ${players[players.length-1].name}.`;
    io.to("race").emit("comment", comment);
    commentTimerId = setInterval(commentTopPlayers, 30000);
}

function commentTopPlayers() {
    let sortPlayers = players.sort(comparePlayers);
    let comment = `${sortPlayers[0].name} зараз перший`;
    for (let i = 1; i < sortPlayers.length; i++) {
        comment += `, за ним ${sortPlayers[i].name} відстає на ${sortPlayers[i-1].keys-sortPlayers[i].keys}`
    }
    comment += '.';
    io.to("race").emit("comment", comment);
}

function commentFinishPlayer(player) {
    const comment = `Гравець ${player.name} досягає фінішу!`;
    io.to("race").emit("comment", comment);
}

function comment30Keys() {
    commented30Keys = true;
    let sortPlayers = players.sort(comparePlayers);
    let comment = `Гонка добігає кінця, гравці на фінішній прямій!\n${sortPlayers[0].name} зараз перший`;
    for (let i = 1; i < sortPlayers.length; i++) {
        comment += `, за ним ${sortPlayers[i].name} відстає на ${sortPlayers[i-1].keys-sortPlayers[i].keys}`
    }
    comment += '.';
    io.to("race").emit("comment", comment);
}

function comparePlayers(p1, p2) {
    if (p1.keys > p2.keys) {
        return -1;
    }
    if (p1.keys < p2.keys) {
        return 1;
    }
    return 0;
}

function disableComments() {
    clearInterval(commentTimerId);
    const sortPlayers = players.sort(comparePlayers);
    const comment = `Перше місце посідає ${sortPlayers[0].name}, друге ${sortPlayers[1].name}`;
    if (sortPlayers.length > 2) {
        comment += `, третє ${sortPlayers[2].name}!`;
    }
    io.to("race").emit("comment", comment);
}

function chooseText() {
    return texts[Math.floor(Math.random() * texts.length)];
}

function startRace() {
    players.forEach(player => player.keys = 0);
    enableComments();
    let text = chooseText();
    curText = text;
    io.to('race').emit('start', {
        players: players,
        text: text
    });
}

function finishRace() {
    disableComments();
    io.to('race').emit('finish');
}